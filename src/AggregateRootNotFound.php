<?php

namespace Maxipost\DomainEventSourcing;

use Exception;

final class AggregateRootNotFound extends Exception implements ButtercupProtectsException
{
}
