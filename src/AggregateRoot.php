<?php

namespace Maxipost\DomainEventSourcing;

abstract class AggregateRoot implements RecordsEvents
{
    /**
     * @var AggregateRootId
     */
    protected $id;
    /**
     * @var array
     */
    private $recordedEvents = [];

    public function __construct(AggregateRootId $id)
    {
        $this->id = $id;
    }

    /**
     * @return \Maxipost\DomainEventSourcing\AggregateRootId
     */
    public function getId(): AggregateRootId
    {
        return $this->id;
    }

    public function getRecordedEvents(): DomainEvents
    {
        return new DomainEvents($this->recordedEvents);
    }

    public function clearRecordedEvents(): void
    {
        $this->recordedEvents = [];
    }

    protected function recordThat(DomainEvent $aDomainEvent): void
    {
        $this->recordedEvents[] = $aDomainEvent;
    }

    protected function applyAndRecordThat(DomainEvent $aDomainEvent): void
    {
        $this->recordThat($aDomainEvent);
        $this->applyEvent($aDomainEvent);
    }

    abstract public function applyEvent(DomainEvent $aDomainEvent): void;
}
