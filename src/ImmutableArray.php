<?php

namespace Maxipost\DomainEventSourcing;

use SplFixedArray;
use function count;

abstract class ImmutableArray extends SplFixedArray
{
    /**
     * Throw when the item is not an instance of the accepted type.
     *
     * @param mixed $item
     *
     * @throws \InvalidArgumentException
     */
    abstract protected function guardType($item): void;

    public function __construct(array $items)
    {
        parent::__construct(count($items));
        $i = 0;
        foreach ($items as $item) {
            $this->guardType($item);
            parent::offsetSet($i++, $item);
        }
    }

    final public function count(): int
    {
        return parent::count();
    }

    /**
     * @return mixed
     */
    final public function current()
    {
        return parent::current();
    }

    final public function key(): int
    {
        return parent::key();
    }

    final public function next(): void
    {
        parent::next();
    }

    final public function rewind(): void
    {
        parent::rewind();
    }

    final public function valid(): bool
    {
        return parent::valid();
    }

    /**
     * @param mixed $offset
     *
     * @return bool
     */
    final public function offsetExists($offset): bool
    {
        return parent::offsetExists($offset);
    }

    /**
     * @param mixed $offset
     *
     * @return mixed
     */
    final public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    final public function offsetSet($offset, $value): void
    {
        throw new ArrayIsImmutable();
    }

    /**
     * @param mixed $offset
     */
    final public function offsetUnset($offset): void
    {
        throw new ArrayIsImmutable();
    }
}
