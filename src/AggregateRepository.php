<?php

namespace Maxipost\DomainEventSourcing;

/**
 * A collection-oriented Repository for eventsourced Aggregates.
 */
interface AggregateRepository
{
    /**
     * @param AggregateRootId $id
     *
     * @return AggregateRoot
     */
    public function get(AggregateRootId $id): AggregateRoot;

    /**
     * @param RecordsEvents $aggregate
     */
    public function persist(RecordsEvents $aggregate): void;
}
