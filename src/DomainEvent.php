<?php

namespace Maxipost\DomainEventSourcing;

abstract class DomainEvent implements DomainEventInterface
{
    /**
     * @var AggregateRootId
     */
    protected $_id;

    public function __construct(AggregateRootId $id)
    {
        $this->_id = $id;
    }

    public function getId(): AggregateRootId
    {
        return $this->_id;
    }
}
