<?php

namespace Maxipost\DomainEventSourcing;

use BadMethodCallException;

final class ArrayIsImmutable extends BadMethodCallException
{
}
