<?php

namespace Maxipost\DomainEventSourcing;

/**
 * An object that records the events that happened to it since the last time it was cleared, or since it was
 * restored from persistence.
 */
interface RecordsEvents
{
    /**
     * Get all the Domain Events that were recorded since the last time it was cleared, or since it was
     * restored from persistence. This does not include events that were recorded prior.
     *
     * @return DomainEvents
     */
    public function getRecordedEvents(): DomainEvents;

    /**
     * Clears the record of new Domain Events. This doesn't clear the history of the object.
     */
    public function clearRecordedEvents(): void;
}
