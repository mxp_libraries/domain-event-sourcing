<?php

namespace Maxipost\DomainEventSourcing;

final class AggregateHistory extends DomainEvents
{
    /**
     * @var AggregateRootId
     */
    private $_id;

    /**
     * AggregateHistory constructor.
     *
     * @param AggregateRootId $id
     * @param array $events
     *
     * @throws CorruptAggregateHistory
     */
    public function __construct(AggregateRootId $id, array $events)
    {
        /** @var DomainEventInterface $event */
        foreach ($events as $event) {
            if (!$event->getId()->equals($id)) {
                throw new CorruptAggregateHistory();
            }
        }
        parent::__construct($events);
        $this->_id = $id;
    }

    /**
     * @return AggregateRootId
     */
    public function getId(): AggregateRootId
    {
        return $this->_id;
    }
}
