<?php

namespace Maxipost\DomainEventSourcing;

use Exception;

final class MultipleInstancesOfAggregateDetected extends Exception implements ButtercupProtectsException
{
}
