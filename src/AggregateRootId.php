<?php

namespace Maxipost\DomainEventSourcing;


use InvalidArgumentException;
use Maxipost\DomainEventSourcing\Exception\InvalidUUIDException;
use Ramsey\Uuid\Uuid;

class AggregateRootId
{
    protected $uuid;

    public function __construct(?string $id = null)
    {
        $this->setUuid($id);
    }

    public function __toString(): string
    {
        return $this->uuid;
    }

    public static function fromString(string $string): self
    {
        return new static($string);
    }

    public function equals(AggregateRootId $aggregateRootId): bool
    {
        return $this->uuid === $aggregateRootId->__toString();
    }

    public function bytes(): string
    {
        return Uuid::fromString($this->uuid)->getBytes();
    }

    public static function fromBytes(string $bytes): self
    {
        return new static(Uuid::fromBytes($bytes)->toString());
    }

    public static function toBytes(string $uid): string
    {
        return (new static($uid))->bytes();
    }

    protected function setUuid(?string $id): void
    {
        try {
            $this->uuid = Uuid::fromString($id ?: Uuid::uuid4()->toString())->toString();
        } catch (InvalidArgumentException $e) {
            throw new InvalidUUIDException();
        }
    }
}
