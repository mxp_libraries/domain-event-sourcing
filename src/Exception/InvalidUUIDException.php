<?php

namespace Maxipost\DomainEventSourcing\Exception;

use InvalidArgumentException;

class InvalidUUIDException extends InvalidArgumentException
{
    public function __construct()
    {
        parent::__construct('aggregator_root.exception.invalid_uuid', 400);
    }
}
