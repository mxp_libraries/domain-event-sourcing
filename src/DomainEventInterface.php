<?php

namespace Maxipost\DomainEventSourcing;

/**
 * Something that happened in the past, that is of importance to the business.
 */
interface DomainEventInterface
{
    /**
     * The Aggregate this event belongs to.
     *
     * @return AggregateRootId
     */
    public function getId(): AggregateRootId;

    /**
     * String event id. For example: order.wasCreated
     * @return string
     */
    public static function getEventId(): string;
}
