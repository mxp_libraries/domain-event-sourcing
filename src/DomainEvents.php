<?php

namespace Maxipost\DomainEventSourcing;

class DomainEvents extends ImmutableArray
{
    protected function guardType($item): void
    {
        if (!($item instanceof DomainEventInterface)) {
            throw new ArrayIsImmutable();
        }
    }
}
