<?php

namespace Maxipost\DomainEventSourcing;

interface Projection
{
    public function project(DomainEvents $eventStream): void;
}
