FROM php:7.1-cli-alpine3.9

RUN set -xe \
    && apk add --no-cache --virtual .build-deps g++ make autoconf \
    && pecl channel-update pecl.php.net \
    && pecl install xdebug-2.7.0RC2 && docker-php-ext-enable xdebug

# |--------------------------------------------------------------------------
# | Composer
# |--------------------------------------------------------------------------
# |
# | Installs Composer to easily manage your PHP dependencies.
# |

ENV COMPOSER_VERSION 1.6.2
ENV COMPOSER_ALLOW_SUPERUSER 1

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=$COMPOSER_VERSION &&\
    chmod +x /usr/local/bin/composer

# |--------------------------------------------------------------------------
# | prestissimo
# |--------------------------------------------------------------------------
# |
# | Installs prestissimo to fix Composer performance issues.
# |

# installs prestissimo for the www-data user.
USER www-data

ENV PRESTISSIMO_VERSION 0.3.7

RUN composer global require hirak/prestissimo:$PRESTISSIMO_VERSION

# done, let's go back to root user!
USER root

ADD . /app

RUN cd /app && composer install && chown -R www-data:www-data  /app/vendor

WORKDIR /app
